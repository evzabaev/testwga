﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Manager : MonoBehaviour {
    public GameObject Buttom;
    public GameObject Target;
    public GameObject Empty;
    public GameObject WinPanel;

    public bool Active; // Показывает была ли нажата клавиша
    public int OldButtom; // Какая клавиша была нажата

    public int a, b;


    void Start () {
        reset();
        SelectTarget();
        FillRandom();
    }

    public void ExitButtom()
    {
        Application.Quit();
    }

    public void ReGameButtom()
    {
        reset();
        SelectTarget();
        FillRandom();
        return;
    }

    public void ButtomBBB(int j)
    {
        //Buttom.transform.GetChild(i).GetComponent<Image>().color = new Color32(0xFF, 0x00, 0x00, 0x9C);
        //Debug.Log("1");

        Color tmp;

        if (Active == false)
        {
            //Buttom.transform.GetChild(j).gameObject.GetComponent<Image>().color = new Color32(0x00, 0x00, 0x00, 0x9C);
            OldButtom = j;
            Active = true;
        }
        else
        {
           // a = j; b = OldButtom;
            if (check(j, OldButtom))
            {
                if (j <= 14)
                {
                    tmp = Buttom.transform.GetChild(j).GetComponent<Image>().color;
                    if (OldButtom <= 14)
                    {
                        Buttom.transform.GetChild(j).GetComponent<Image>().color = Buttom.transform.GetChild(OldButtom).GetComponent<Image>().color;
                        Buttom.transform.GetChild(OldButtom).GetComponent<Image>().color = tmp;
                    }
                    else
                    {
                        Buttom.transform.GetChild(j).GetComponent<Image>().color = Empty.transform.GetChild(OldButtom - 15).GetComponent<Image>().color;
                        Empty.transform.GetChild(OldButtom - 15).GetComponent<Image>().color = tmp;
                    }
                }
                else
                {
                    tmp = Empty.transform.GetChild(j - 15).GetComponent<Image>().color;
                    if (OldButtom <= 14)
                    {
                        Empty.transform.GetChild(j - 15).GetComponent<Image>().color = Buttom.transform.GetChild(OldButtom).GetComponent<Image>().color;
                        Buttom.transform.GetChild(OldButtom).GetComponent<Image>().color = tmp;
                    }
                    else
                    {
                        Buttom.transform.GetChild(j - 15).GetComponent<Image>().color = Empty.transform.GetChild(OldButtom - 15).GetComponent<Image>().color;
                        Empty.transform.GetChild(OldButtom - 15).GetComponent<Image>().color = tmp;
                    }
                }
            }
            reset();
        }

        if (Check_win())
        {
            WinPanel.active = true;
        }

        return;
    }

    private bool Check_win()
    {
        for (int i = 0; i < 5; ++i)
        {
            if (Buttom.transform.GetChild(i).GetComponent<Image>().color != Target.transform.GetChild(0).GetComponent<Image>().color)
                return false;
        }
        for (int i = 5; i < 10; ++i)
        {
            if (Buttom.transform.GetChild(i).GetComponent<Image>().color != Target.transform.GetChild(1).GetComponent<Image>().color)
                return false;
        }
        for (int i = 10; i < 15; ++i)
        {
            if (Buttom.transform.GetChild(i).GetComponent<Image>().color != Target.transform.GetChild(2).GetComponent<Image>().color)
                return false;
        }
        return true;
    }

    private bool check(int i, int j)
    {
        if ((i < 5) && (j < 5))
        {
            if ((i == (j - 1)) || (i == (j + 1)))
                return true;
            return false;
        }
        else if ((i >= 10) && (i < 15) && (j >= 10) && (j < 15))
        {
            if ((i == (j - 1)) || (i == (j + 1)))
                return true;
            return false;
        }
        else if ((i < 10) && (i >= 5) && (j < 10) && (j >= 5))
        {
            if ((i == (j - 1)) || (i == (j + 1)))
                return true;
            return false;
        }
        else if ((i > 14) && (j <= 14))
        {
            if (i == 15)
            {
                if ((j == 1) || (j == 6))
                    return true;
                return false;
            }
            if (i == 16)
            {
                if ((j == 6) || (j == 11))
                    return true;
                return false;
            }
            if (i == 17)
            {
                if ((j == 3) || (j == 8))
                    return true;
                return false;
            }
            if (i == 18)
            {
                if ((j == 8) || (j == 13))
                    return true;
                return false;
            }
        }
        else if ((j > 14) && (i <= 14))
        {
            if (j == 15)
            {
                if ((i == 1) || (i == 6))
                    return true;
                return false;
            }
            if (j == 16)
            {
                if ((i == 6) || (i == 11))
                    return true;
                return false;
            }
            if (j == 17)
            {
                if ((i == 3) || (i == 8))
                    return true;
                return false;
            }
            if (j == 18)
            {
                if ((i == 8) || (i == 13))
                    return true;
                return false;
            }
        }

        return false;
    }

    private void FillRandom() // Заполнение сетки 5х5
    {
        GameObject[] pu = GameObject.FindGameObjectsWithTag("Buttom");
        int a = 0, b = 0, c = 0, rand = 0 ;

        for (int i = 0; i < Buttom.transform.GetChildCount(); i++)
        {
            rand = Random.Range((int)1,(int)(15 - (a + b + c)));

            if (rand <= (5 - a))
            {
                Buttom.transform.GetChild(i).GetComponent<Image>().color = new Color32(0xFF, 0x00, 0x00, 0x9C);
                ++a;
            }
            else if (((5 - a) < rand) && (rand <= (10 - (b + a))))
            {
                Buttom.transform.GetChild(i).GetComponent<Image>().color = new Color32(0x00, 0xFF, 0x00, 0x9C);
                ++b;
            }
            else
            {
                Buttom.transform.GetChild(i).GetComponent<Image>().color = new Color32(0x00, 0x00, 0xFF, 0x9C);
                ++c;
            }

        }
    }
	
    private void SelectTarget() // Выбор каким цветом заполнять столбец
    {
        //int number_1 = Random.Range((int)1,(int)3), number_2 = 0, number_3 = 0;
        int[] number = new int[3];

        number[1] = Random.Range((int)1, (int)4);

        while (true)
        {
            if ((number[2] = Random.Range((int)1, (int)4)) != number[1])
                break;
        }

        number[0] = 6 - (number[1] + number[2]);

        for (int i = 0; i < 3; ++i)
        {
            if (number[i] == 1)
                Target.transform.GetChild(i).GetComponent<Image>().color = new Color32(0xFF, 0x00, 0x00, 0x9C);
            if (number[i] == 2)
                Target.transform.GetChild(i).GetComponent<Image>().color = new Color32(0x00, 0xFF, 0x00, 0x9C);
            if (number[i] == 3)
                Target.transform.GetChild(i).GetComponent<Image>().color = new Color32(0x00, 0x00, 0xFF, 0x9C);
        }
    }

    private void reset()
    {
        Active = false;
        OldButtom = 0;
        WinPanel.active = false;
    }

}
